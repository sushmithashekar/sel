from selenium import webdriver
from selenium.webdriver.common.keys import *
import time


driver = webdriver.Chrome(executable_path="C:\Drivers\chromedriver_win32 (3)\chromedriver")
driver.get("http://demo.automationtesting.in/Windows.html")
print(driver.title)
print(driver.current_url)
driver.find_element_by_xpath("//*[@id='Tabbed']/a/button").click()
#driver.close()  # closes only the current browser
time.sleep(5)
driver.quit()   # closes all the browsers