import unittest
from Package1.TC_Logintest import LoginTest
from Package1.TC_SignupTest import SignupTest

from package2.Tc_PaymentreturnsTest import paymentReturnsTest
from package2.TC_PaymentTest import PaymentTest

#get all the test methods from all the testcases

tc1 = unittest.TestLoader().loadTestsFromTestCase(LoginTest)
tc2 = unittest.TestLoader().loadTestsFromTestCase(SignupTest)
tc3 = unittest.TestLoader().loadTestsFromTestCase(PaymentTest)
tc4 = unittest.TestLoader().loadTestsFromTestCase(paymentReturnsTest)

#craeting test suites

sanitytestsuites = unittest.TestSuite([tc1,tc2])
functionaltestsuit = unittest.TestSuite([tc3,tc4])
Mastertestsuite = unittest.TestSuite([tc1,tc2,tc3,tc4])

#unittest.TextTestRunner().run(sanitytestsuites)    #sanity suites
#unittest.TextTestRunner().run(functionaltestsuit)    #Functional test suite
unittest.TextTestRunner(verbosity=2).run(Mastertestsuite)  #Master test suite


