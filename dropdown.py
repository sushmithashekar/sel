from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import time

driver = webdriver.Chrome()
driver.maximize_window()
driver.get("https://www.facebook.com/campaign/landing.php?campaign_id=1653993517&extra_1=s%7Cc%7C355887219764%7Ce%7Ccreate%20new%20account%20facebook%7C&placement=&creative=355887219764&keyword=create%20new%20account%20facebook&partner_id=googlesem&extra_2=campaignid%3D1653993517%26adgroupid%3D63066386563%26matchtype%3De%26network%3Dg%26source%3Dnotmobile%26search_or_content%3Ds%26device%3Dc%26devicemodel%3D%26adposition%3D%26target%3D%26targetid%3Dkwd-302197463940%26loc_physical_ms%3D1007768%26loc_interest_ms%3D%26feeditemid%3D%26param1%3D%26param2%3D&gclid=EAIaIQobChMI_avYxpno6gIVUgwrCh1m-QlyEAAYASAAEgJaffD_BwE")

day = driver.find_element_by_name("birthday_day")
bday = Select(day)
# selecting by visible text
bday.select_by_visible_text('3')

# selecting by index
month= driver.find_element_by_name("birthday_month")
bmonth=Select(month)

bmonth.select_by_index(4)

#select by value
year = driver.find_element_by_name("birthday_year")
byear=Select(year)

byear.select_by_value("1994")



#print the number of options
print(len(bday.options))

#display all options

for options in bday.options:
    print(options.text)

print(len(bmonth.options))

for options in bmonth.options:
    print(options.text)

print(len(byear.options))

for options in byear.options:
    print(options.text)















